"""In this module are defined all the groups."""
from collections import namedtuple
from typing import NamedTuple

from libqtile.config import DropDown, Group, ScratchPad

from .my_vars import my_variables as my
from .utils import BetterScratch


GroupTemplate = namedtuple("GroupTemplate", "name label layout")

group_templates = [
    GroupTemplate("1", "", "bsp"),
    GroupTemplate("2", "", "monadtall"),
    GroupTemplate("3", "切", "bsp"),
    GroupTemplate("4", "", "bsp"),
    GroupTemplate("5", "", "bsp"),
    GroupTemplate("6", "", "bsp"),
    GroupTemplate("7", "", "bsp"),
    GroupTemplate("8", "", "bsp"),
    GroupTemplate("9", "侮", "treetab"),
    GroupTemplate("0", "", "floating"),
]

DropTemplate = namedtuple("DropTemplate", "name cmd kwargs")

base_drowdown_args = {
    "x": 0.05,
    "y": 0.2,
    "width": 0.6,
    "height": 0.55,
    "opacity": 0.9,
    # INFO: Am lăsat tupa po pricolu
    "on_focus_lost_hide": True,
}


class MyDropdowns(NamedTuple):
    """Holds the name and comand to run for all the available DropDowns"""

    telegram: DropTemplate = DropTemplate(
        "telegram",
        f"{my.telegram}",
        dict(
            base_drowdown_args,
            x=0.13,
            y=0.17,
            height=0.65,
            on_focus_lost_hide= False,
             ),
    )
    term_trustworthy: DropTemplate = DropTemplate(
        "term_trustworthy",
        my.term,
        # INFO: Dacă o setare din bază nu ne confive o putem modifica așa
        dict(
            base_drowdown_args,
            x=0.2,
            y=0.15,
            height=0.65,
            on_focus_lost_hide=False,
        ),
    )
    term: DropTemplate = DropTemplate(
        "term",
        my.term,
        # INFO: Dacă o setare din bază nu ne confive o putem modifica așa
        dict(
            base_drowdown_args,
            x=0.2,
            y=0.15,
            height=0.65,
            on_focus_lost_hide=True,
        ),
    )
    browser: DropTemplate = DropTemplate(
        "browser",
        my.browser2,
        # INFO: Dacă o setare din bază nu ne confive o putem modifica așa
        dict(
            base_drowdown_args,
            x=0.2,
            y=0.15,
            height=0.65,
            on_focus_lost_hide=True,
        ),
    )

dynamic_scratch = BetterScratch(group_name="better_scratch")

MY_GROUPS = [
    Group(name=name, layout=layout, label=label)
    for name, label, layout in group_templates
] + [
    ScratchPad(
        my.scratchpad,
        [DropDown(name, cmd, **kwargs) for name, cmd, kwargs in MyDropdowns()],
    )
]

GROUP_SPECIFIC_APPS = {
    "1": [],
    "2": [
        # "qutebrowser",
        "Navigator",
        "brave-browser",
    ],
    "3": [],
    "4": [],
    "5": [],
    "6": [],
    "7": [
        "gimp",
        "gimp-2.10",
        "inkscape",
    ],
    "8": [ ],
    "9": [ ],
    "0": [
        "Mail",
        "mail",
        "thunderbird",
        "Thunderbird",
        "Pragha",
        "Clementine",
        "evolution",
        "Deadbeef",
        "Audacious",
        "pragha",
        "clementine",
        "deadbeef",
        "audacious",
    ],
}
