"""In this module are defined all the custom variables."""
from collections import namedtuple
from typing import NamedTuple
from os.path import expanduser


Color = namedtuple("Color", "first second")
class MyColors(NamedTuple):
    """A class to keep al the custom colors"""
    color0: Color = Color("#2F343F", "#2F343F")
    color1: Color = Color("#2F343F", "#2F343F")
    color2: Color = Color("#c0c5ce", "#c0c5ce")
    color3: Color = Color("#ff5050", "#ff5050")
    color4: Color = Color("#f4c2c2", "#f4c2c2")
    color5: Color = Color("#ffffff", "#ffffff")
    color6: Color = Color("#ffd47e", "#ffd47e")
    color7: Color = Color("#62FF00", "#62FF00")
    color8: Color = Color("#000000", "#000000")
    color9: Color = Color("#c40234", "#c40234")
    color10: Color = Color("#6790eb", "#6790eb")
    color11: Color = Color("#ff00ff", "#ff00ff")
    color12: Color = Color("#4c566a", "#4c566a")
    color13: Color = Color("#282c34", "#282c34")
    color14: Color = Color("#212121", "#212121")
    color15: Color = Color("#e75480", "#e75480")
    color16: Color = Color("#2aa899", "#2aa899")
    color17: Color = Color("#abb2bf", "#abb2bf")
    color18: Color = Color("#81a1c1", "#81a1c1")
    color19: Color = Color("#56b6c2", "#56b6c2")
    color20: Color = Color("#b48ead", "#b48ead")
    color21: Color = Color("#e06c75", "#e06c75")
    color22: Color = Color("#fb9f7f", "#fb9f7f")
    color23: Color = Color("#ffd47e", "#ffd47e")


class MyVariables(NamedTuple):
    """A class to keep my custom variables"""
    home = expanduser("~")
    term: str = "alacritty"
    term2: str = "kitty"
    gui_editor1: str = f"neovide --neovim-bin {home}/.local/bin/lvim "
    our_editor: str = "emacs"
    mod: str = "mod4"
    browser: str = "firedragon"
    browser2: str = "qutebrowser"
    browser3: str = "brave"
    colors: MyColors = MyColors()
    scratchpad: str = "SPD"
    telegram: str = "telegram-desktop"


my_variables = MyVariables()
