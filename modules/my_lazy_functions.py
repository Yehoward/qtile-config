from libqtile.command import lazy

@lazy.function
def window_to_prev_group(_qtile):
    "Moves the active window to the previous group."
    if _qtile.currentWindow is not None:
        i = _qtile.groups.index(_qtile.currentGroup)
        _qtile.currentWindow.togroup(_qtile.groups[i - 1].name)


@lazy.function
def window_to_next_group(_qtile):
    "Moves the active window to the next group."
    if _qtile.currentWindow is not None:
        i = _qtile.groups.index(_qtile.currentGroup)
        _qtile.currentWindow.togroup(_qtile.groups[i + 1].name)

