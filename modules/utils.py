"""
The MIT License (MIT)

Copyright (c) 2021 James Wright

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import sys
import subprocess as sproc
import os

import libqtile
import libqtile.config
from libqtile.lazy import lazy
from libqtile.log_utils import logger

# For type hints
from libqtile.core.manager import Qtile
from libqtile.group import _Group
from libqtile.backend import base
from collections.abc import Callable


class BetterScratch(object):
    """For creating a mutable scratch workspace (similar to i3's scratch functionality)"""


    def __init__(self, win_attr: str='mutscratch', group_name: str=''):
        """

        Parameters
        ----------
        win_attr : str
            Attribute added to the qtile.window object to determine whether the
            window is a part of the MutableScratch system
        group_name : str
            Name of the group that holds the windows added to the scratch space
        """

        self.win_attr: str = win_attr
        self.scratch_group_name: str = group_name

        self.win_stack: list = [] # Equivalent of focus_history


    def qtile_startup(self):
        """Initialize MutableScratch group on restarts

        Put
            hook.subscribe.startup_complete(<MutScratch>.qtile_startup)
        in your config.py to initialize the windows in the MutScratch group
        """

        qtile = libqtile.qtile
        group = qtile.groups_map[self.scratch_group_name]

        for win in group.windows:
            win.floating = True
            setattr(win, self.win_attr, True)

        self.win_stack = group.windows.copy()


    def add_current_window(self) -> Callable:
        """Add current window to the MutableScratch system"""
        @lazy.function
        def _add_current_window(qtile: Qtile):
            if qtile.current_window is not None:
                win: base.Window = qtile.current_window
                win.hide()
                win.floating = True
                setattr(win, self.win_attr, True)

                win.togroup(self.scratch_group_name)
                self.win_stack.append(win)

        return _add_current_window


    def remove_current_window(self) -> Callable:
        """Remove current window from MutableScratch system"""
        @lazy.function
        def _remove(qtile: Qtile):
            win = qtile.current_window
            setattr(win, self.win_attr, False)

            if win in self.win_stack:
                self.win_stack.remove(win)
        return _remove


    def toggle(self) -> Callable:
        """Toggle between hiding/showing MutableScratch windows

        If current window is in the MutableScratch system, hide the window. If
        it isn't, show the next window in the stack.
        """
        @lazy.function
        def _toggle(qtile: Qtile):
            if qtile.current_window is not None:
                win: base.Window = qtile.current_window
                if getattr(win, self.win_attr, False):
                    self._push(win)
                else:
                    self._pop(qtile)
        return _toggle


    def _push(self, win: base.Window) -> None:
        """Hide and push window to stack

        Parameters
        ----------
        win : libqtile.backend.base.Window
            Window to push to the stack
        """
        win.togroup(self.scratch_group_name)
        self.win_stack.append(win)


    def _pop(self, qtile: Qtile) -> None:
        """Show and pop window from stack

        Parameters
        ----------
        qtile : libqtile.qtile
            qtile root object
        win : libqtile.backend.base.Window
            Window to pop from stack
        """
        scratch_group: _Group = qtile.groups_map[self.scratch_group_name]
        if set(self.win_stack) != set(scratch_group.windows):
            logger.warning(f"{self}'s win_stack and {scratch_group}'s windows have mismatching windows: "
                           f"{set(self.win_stack).symmetric_difference(set(scratch_group.windows))}")
            self.win_stack = scratch_group.windows.copy()
        if self.win_stack:
            win = self.win_stack.pop(0)
            win.togroup(qtile.current_group.name)


class TKHints:
    proc: sproc.Popen | None = None
    tkhints: str = os.path.expanduser("~/Documents/prg/tkhints/tkhints.py")

    def show(self, stdin_value: str) -> None:

        logger.warn("Starting TKHinter")

        self.proc = sproc.Popen(self.tkhints, text=True, stdin=sproc.PIPE, stdout=sys.stdout)
        try:
            self.proc.communicate(input=stdin_value, timeout=0.1)
        except sproc.TimeoutExpired:
            logger.info("TKHinter timeout expired, hope it closes on chord leave :D")

    def kill(self) -> None:
        if self.proc is not None:
            logger.info("Closing TKHinter")
            self.proc.kill()

    def __del__(self) -> None:
        if self.proc is not None and self.proc.poll() is None:
            self.proc.kill()

