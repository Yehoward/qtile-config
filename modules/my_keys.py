"""In this module are defined all the KeyBinds and KeyChords."""
from collections import namedtuple
from libqtile.command import lazy
from libqtile.config import Key, KeyChord

from typing import NamedTuple

from .my_vars import my_variables as my
from .my_groups import MyDropdowns, dynamic_scratch
from .my_layouts import MyLayouts

# home = os.path.expanduser("~")

KeyTemplate = namedtuple("KeyTemplate", "key actions description")


class KeyChordTemplate(NamedTuple):
    key: str
    name: str
    description: str
    actions: list[KeyTemplate]
    mode: bool = False


# Define and use constants for frequently used keys so the lsp will catch errors
CTRL = "control"
ALT = "mod1"
SHIFT = "shift"
home = my.home

MDD = MyDropdowns()
ML = MyLayouts()

key_templates = [

    # Open apps
    KeyChordTemplate(
        key="o",
        name="open",
        description="[O]pen apps",
        actions=[
            KeyTemplate(
                key="f",
                description="firedragon",
                actions=[lazy.spawn("firedragon")],
            ),
            KeyTemplate(
                key="b",
                description="brave",
                actions=[lazy.spawn("brave")],
            ),
            KeyTemplate("m", [lazy.spawn("pcmanfm")], "pcmanfm"),
            KeyTemplate(
                key="w",
                description="garuda-welcome",
                actions=[lazy.spawn("garuda-welcome"), lazy.ungrab_chord()],
            ),
            KeyTemplate(
                key="p",
                description="pamac-manager",
                actions=[lazy.spawn("pamac-manager"), lazy.ungrab_chord()],
            ),
            KeyTemplate(
                key="t",
                description="telegram-desktop",
                actions=[lazy.spawn("telegram-desktop"), lazy.ungrab_chord()],
            ),
            KeyTemplate(
                key="s",
                description="Session",
                actions=[lazy.spawn("session-messenger-desktop"), lazy.ungrab_chord()],
            ),
            KeyTemplate(
                key="g",
                description="gimp",
                actions=[lazy.spawn("flatpak run org.gimp.GIMP")],
            ),
            KeyTemplate(
                key="n",
                description="neovide",
                actions=[lazy.spawn("neovide")],
            ),
            KeyTemplate(
                key="l",
                description="neovide lvim",
                actions=[
                    lazy.spawn(my.gui_editor1),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="q",
                description="qutebrowser",
                actions=[lazy.spawn("qutebrowser")],
            ),
        ],
    ),

    # Screen capture and record
    KeyChordTemplate(
        key="p",
        name="screenshot",
        description="[P]rint the screen (or as millenials say take a screenshot)",
        actions=[
            KeyTemplate(
                key="f",
                description="Copy full screen to clipboard",
                actions=[
                    lazy.spawn("flameshot full --clipboard "),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="g",
                description="Flameshot gui",
                actions=[lazy.spawn("flameshot gui"), lazy.ungrab_chord()],
            ),
            KeyTemplate(
                key="s",
                description="Save full screen",
                actions=[
                    lazy.spawn(f"flameshot full -p {home}/Pictures/screenshot"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="r",
                description="Record video",
                actions=[
                    lazy.spawn(home + "/.local/bin/scr_rec"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="o",
                description="Open pictures folder",
                actions=[lazy.spawn(f"alacritty -e ranger {home}/Pictures")],
            ),
            KeyTemplate(
                "v",
                [lazy.spawn(f"alacritty -e ranger {home}/Videos/")],
                "Open Video folder",
            ),
        ],
    ),

    # General key binds
    KeyChordTemplate(
        key="g",
        name="general",
        description="Random bulshit [g]o",
        actions=[
            KeyTemplate(
                key="w",
                description="Random Wallpaper",
                actions=[
                    lazy.spawn(home + "/.config/qtile/scripts/bg/rand_bg"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="k",
                description="Shakespear insults",
                actions=[
                    lazy.spawn(home + "/Documents/my_repos/yeho-scripts/usefull/shake_insult_voice"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="t",
                description="Toggle touchpad",
                actions=[
                    lazy.spawn(home + "/.config/qtile/scripts/touch_toggle"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="s",
                description="Search stuff",
                actions=[
                    lazy.spawn(home + "/.config/qtile/scripts/search_net"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="m",
                description="watch copied link",
                actions=[
                    lazy.spawn(home+"/.local/bin/mpw"),
                    lazy.ungrab_chord(),
                ],
            ),
            KeyTemplate(
                key="i",
                actions=[
                    lazy.spawn(home + "/.config/qtile/scripts/bg/rand_bg -light"),
                    lazy.ungrab_chord(),
                ],
                description="Random Light Wallpaper",
            ),
            KeyTemplate(
                key="r",
                description="Reload config",
                actions=[lazy.restart()],
            ),
            KeyTemplate(
                key="q",
                description="Open qtile config",
                actions=[
                    lazy.spawn(f"{my.gui_editor1} {home}/.config/qtile/"),
                    lazy.ungrab_chord(),
                ],
            ),
            # KeyTemplate(
            #     key="k",
            #     description="Open qtile keyconfig",
            #     actions=[
            #         lazy.spawn(f"{my.gui_editor1} {home}/.config/qtile/modules/my_keys.py"),
            #         lazy.ungrab_chord(),
            #     ],
            # ),
            KeyTemplate(
                key="p",
                description="Toggle polybar",
                actions=[
                    lazy.spawn(home + "/.config/qtile/scripts/polytoggle"),
                    lazy.ungrab_chord(),
                ],
            ),
        ],
    ),

    # Exit
    KeyChordTemplate(
        key="x",
        name="exit",
        description="E[x]it",
        actions=[
            KeyTemplate("x", [lazy.shutdown()], "Log out"),
            KeyTemplate("p", [lazy.spawn("poweroff")], "poweroff"),
            KeyTemplate("r", [lazy.spawn("reboot")], "reboot"),
        ],
    ),

    # Change keyboard layout
    KeyChordTemplate(
        key="F1",
        name="keyboard_layout",
        description="Layout",
        actions=[
            KeyTemplate(
                "1",
                [lazy.spawn("setxkbmap -layout ro")],
                "Romîna",
            ),
            KeyTemplate("2", [lazy.spawn("setxkbmap -layout us")], "English qwerty"),
            KeyTemplate(
                "3",
                [lazy.spawn("setxkbmap -layout ro -variant ergonomic")],
                "Romîna ergonomic",
            ),
            KeyTemplate(
                "4",
                [lazy.spawn("setxkbmap -layout ru -variant phonetic")],
                "Русский фонетический",
            ),
            KeyTemplate(
                "5",
                [lazy.spawn("setxkbmap -layout us -variant dvorak")],
                "English dvorak",
            ),
        ],
    ),

    # KeyChordTemplate(
    #     key="r",
    #     name="ran_file",
    #     description="Open a file in [r]anger",
    #     actions=[
    #         KeyTemplate(
    #             "c",
    #             [
    #                 lazy.spawn(f"alacritty -e ranger {home}/.config/qtile/"),
    #                 lazy.ungrab_chord(),
    #             ],
    #             "Open qtile config folder",
    #         ),
    #         KeyTemplate(
    #             "d",
    #             [lazy.spawn(f"alacritty -e ranger {home}/Documents/")],
    #             "Open Documents folder",
    #         ),
    #         KeyTemplate(
    #             "w",
    #             [lazy.spawn(f"alacritty -e ranger {home}/Downloads/")],
    #             "Open Downloads folder",
    #         ),
    #         KeyTemplate(
    #             "h", [lazy.spawn(f"alacritty -e ranger {home}/")], "Open home folder"
    #         ),
    #     ],
    # ),

    # Change tiling layout
    KeyChordTemplate(
        key="t",
        name="layout",
        description="Select a [t]iling layout",
        actions=[
            KeyTemplate("m", [lazy.to_layout_index(ML.monad_tall.id)], "MonadTall"),
            KeyTemplate("x", [lazy.to_layout_index(ML.max.id)], "Maximize"),
            KeyTemplate("c", [lazy.to_layout_index(ML.columns.id)], "Columns"),
            KeyTemplate("b", [lazy.to_layout_index(ML.bsp.id)], "BSP"),
            KeyTemplate("t", [lazy.to_layout_index(ML.treetab.id)], "TreeTab"),
            KeyTemplate("z", [lazy.to_layout_index(ML.zoomy.id)], "Zoomy"),
        ],
    ),

    # Notification
    KeyChordTemplate(
        key="n",
        name="notifications",
        description="Control [n]otifications",
        actions=[
            KeyTemplate("c", [lazy.spawn("dunstctl close-all")], "Close all notifications"),
            KeyTemplate("h", [lazy.spawn("dunstctl history-pop")], "Show history"),
            KeyTemplate("a", [lazy.spawn("dunstctl action")], "Action"),
            KeyTemplate("t", [lazy.spawn("dunstctl context")], "Context menu"),
        ],
    ),

    # ScratchPad
    KeyChordTemplate(
        key="s",
        name="ScratchPads",
        description="[S]elect a [S]cratchPad",
        actions=[
            KeyTemplate(
                key="A",
                description="[A]dd to dynamic scratch",
                actions=[dynamic_scratch.add_current_window()],
            ),
            KeyTemplate(
                key="k",
                description="[K]oll the dynamic scratchpads",
                actions=[dynamic_scratch.toggle()],
            ),
            KeyTemplate(
                key="q",
                description="[Q]uits window from dsp",
                actions=[dynamic_scratch.remove_current_window()],
            ),
            KeyTemplate(
                key="G",
                description="Tele[g]ram ScratchPad",
                actions=[lazy.group[my.scratchpad].dropdown_toggle(MDD.telegram.name)],
            ),
            KeyTemplate(
                key="t",
                description="Trustworthy terminal",
                actions=[lazy.group[my.scratchpad].dropdown_toggle(MDD.term_trustworthy.name)],
            ),
            KeyTemplate(
                key="c",
                description="Dropdown [c]ommandline",
                actions=[lazy.group[my.scratchpad].dropdown_toggle(MDD.term.name)],
            ),
            KeyTemplate(
                key="b",
                description="Quick Browser",
                actions=[lazy.group[my.scratchpad].dropdown_toggle(MDD.browser.name)],
            ),
        ],
    ),
]

key_mods = [
    KeyChord(
        [my.mod],
        key="m",
        name="move_resize",
        submappings=[
            Key([], "l", lazy.window.move_floating(10, 0),),
            Key([], "h", lazy.window.move_floating(-10, 0),),
            Key([], "k", lazy.window.move_floating(0, -10),),
            Key([], "j", lazy.window.move_floating(0, 10),),
            Key([], "s", lazy.window.resize_floating(-5, 0),),
            Key([], "d", lazy.window.resize_floating(0, -5),),
            Key([], "f", lazy.window.resize_floating(0, 5),),
            Key([], "g", lazy.window.resize_floating(5, 0),),
        ],
        mode=True,
    ),
]

mod_control = [
    Key(
        [my.mod, CTRL],
        "o",
        lazy.spawn(home + "/.config/qtile/scripts/picom-toggle.sh"),
    ),
]

mod_keys = [
    Key([my.mod], "f", lazy.window.toggle_fullscreen()),
    # Key([mod], "m", lazy.window.toggle_minimize()),
    Key([my.mod], "q", lazy.window.kill()),
    Key([my.mod], "Return", lazy.spawn(my.term)),
    Key([my.mod], "Escape", lazy.spawn("kitty --hold -e xprop")),
    Key([my.mod], "d", lazy.spawn(home + "/.config/qtile/scripts/menu")),
    Key([my.mod], "period", lazy.next_screen()),
]

mod_shift = [
    Key([my.mod, SHIFT], "d", lazy.spawn("nwggrid -p -o 0.4")),
    Key([my.mod, SHIFT], "Return", lazy.spawn(my.term2)),
    Key([my.mod, SHIFT], "Escape", lazy.spawn("xkill")),
]

media_keys = [
    # INCREASE/DECREASE BRIGHTNESS
    Key([], "XF86MonBrightnessUp", lazy.spawn("brightnessctl s +1%")),
    Key([], "XF86MonBrightnessDown", lazy.spawn("brightnessctl s 1%- ")),
    # INCREASE/DECREASE/MUTE VOLUME
    Key([], "XF86AudioMute", lazy.spawn("amixer -q set Master toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -D pulse sset Master 5%-")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -D pulse sset Master 5%+")),
    Key([], "XF86AudioPlay", lazy.spawn("playerctl play-pause")),
    Key([], "XF86AudioNext", lazy.spawn("playerctl next")),
    Key([], "XF86AudioPrev", lazy.spawn("playerctl previous")),
    Key([], "XF86AudioStop", lazy.spawn("playerctl stop")),
]

layout_keys = [
    # QTILE LAYOUT KEYS
    Key([my.mod], "n", lazy.layout.normalize()),
    Key([my.mod], "space", lazy.next_layout()),
    Key([my.mod], "Tab", lazy.screen.next_group()),
    Key([my.mod, SHIFT], "Tab", lazy.screen.prev_group()),
    # CHANGE FOCUS
    Key([my.mod], "Up", lazy.layout.up()),
    Key([my.mod], "Down", lazy.layout.down()),
    Key([my.mod], "Left", lazy.layout.left()),
    Key([my.mod], "Right", lazy.layout.right()),
    Key([my.mod], "k", lazy.layout.up()),
    Key([my.mod], "j", lazy.layout.down()),
    Key([my.mod], "h", lazy.layout.left()),
    Key([my.mod], "l", lazy.layout.right()),
    # RESIZE UP, DOWN, LEFT, RIGHT
    Key(
        [my.mod, CTRL],
        "l",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [my.mod, CTRL],
        "Right",
        lazy.layout.grow_right(),
        lazy.layout.grow(),
        lazy.layout.increase_ratio(),
        lazy.layout.delete(),
    ),
    Key(
        [my.mod, CTRL],
        "h",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [my.mod, CTRL],
        "Left",
        lazy.layout.grow_left(),
        lazy.layout.shrink(),
        lazy.layout.decrease_ratio(),
        lazy.layout.add(),
    ),
    Key(
        [my.mod, CTRL],
        "k",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [my.mod, CTRL],
        "Up",
        lazy.layout.grow_up(),
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
    ),
    Key(
        [my.mod, CTRL],
        "j",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    Key(
        [my.mod, CTRL],
        "Down",
        lazy.layout.grow_down(),
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
    ),
    # FLIP LAYOUT FOR MONADTALL/MONADWIDE
    Key([my.mod, SHIFT], "f", lazy.layout.flip()),
    # FLIP LAYOUT FOR BSP
    Key([my.mod, ALT], "k", lazy.layout.flip_up()),
    Key([my.mod, ALT], "j", lazy.layout.flip_down()),
    Key([my.mod, ALT], "l", lazy.layout.flip_right()),
    Key([my.mod, ALT], "h", lazy.layout.flip_left()),
    # MOVE WINDOWS UP OR DOWN BSP LAYOUT
    Key([my.mod, SHIFT], "k", lazy.layout.shuffle_up()),
    Key([my.mod, SHIFT], "j", lazy.layout.shuffle_down()),
    Key([my.mod, SHIFT], "h", lazy.layout.shuffle_left()),
    Key([my.mod, SHIFT], "l", lazy.layout.shuffle_right()),
    # Treetab controls
    Key(
        [my.mod, CTRL],
        "k",
        lazy.layout.section_up(),
        desc="Move up a section in treetab",
    ),
    Key(
        [my.mod, CTRL],
        "j",
        lazy.layout.section_down(),
        desc="Move down a section in treetab",
    ),
    # MOVE WINDOWS UP OR DOWN MONADTALL/MONADWIDE LAYOUT
    Key([my.mod, SHIFT], "Up", lazy.layout.shuffle_up()),
    Key([my.mod, SHIFT], "Down", lazy.layout.shuffle_down()),
    Key([my.mod, SHIFT], "Left", lazy.layout.swap_left()),
    Key([my.mod, SHIFT], "Right", lazy.layout.swap_right()),
    # TOGGLE FLOATING LAYOUT
    Key([my.mod, SHIFT], "space", lazy.window.toggle_floating()),
]

key_chords = [
    KeyChord(
        [my.mod],
        key,
        [Key([], s_key, *action) for s_key, action, _ in keys_list],
        name=name,
        mode=mode,
    )
    for key, name, _, keys_list, mode in key_templates
]

my_static_keys = (
    mod_keys + mod_control + media_keys + layout_keys + key_chords + mod_shift + key_mods
)


def new_layout_keys(name: str) -> list[Key]:
    """
    Creates a key to move to, to move the window to, the group with the given name
    """
    return [  # CHANGE WORKSPACES
        Key([my.mod], name, lazy.group[name].toscreen()),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key([my.mod, CTRL], name, lazy.window.togroup(name)),
        # MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key(
            [my.mod, SHIFT],
            name,
            lazy.window.togroup(name),
            lazy.group[name].toscreen(),
        ),
    ]
