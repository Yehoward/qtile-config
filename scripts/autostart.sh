#!/usr/bin/env bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}


#starting utility applications at boot time
lxsession &
run nm-applet &
run pamac-tray &
numlockx on &
blueman-applet &
#mpv --end=2 $HOME/.config/qtile/sfx/eagle_sfx.m4a &
#flameshot &
$HOME/.config/polybar/launch.sh &
picom --config $HOME/.config/picom/picom.conf &
# picom --config .config/picom/picom-blur.conf --experimental-backends &
#/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 &
dunst &
# feh --randomize --bg-fill /usr/share/wallpapers/garuda-wallpapers/*
killall mcron 
mcron -d
$HOME/.config/qtile/scripts/bg/rand_bg &
setxkbmap -option caps:swapescape
#starting user applications at boot time
# run volumeicon &
